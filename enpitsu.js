const fs = require('fs'),
      config = require('./lib/configman.js').config,
      help = require('./lib/help.js').help,
      usage = require('./lib/help.js').usage,
      argv = require('minimist')(process.argv.slice(2)),
      show = require('./lib/show'),
      TaskManager = require('./lib/classes/taskmanager.js');

const mode = process.argv[2];
const cmd = process.argv[3];

const taskmgr = new TaskManager();

// Main
switch (mode) {
  case "h":
  case "help":
  case "--help":
  case "-h":
    help(cmd);
    process.exit();
    break;
  case 'a':
  case 'add':
    taskmgr.addTask(cmd);
    break;
  case 'r':
  case 'rm':
  case 'remove':
    taskmgr.removeTask(cmd);
    break;
  case 'e':
  case 'edit':
    taskmgr.editEntry(cmd);
    break;
  case 'mv':
  case 'move':
    taskmgr.moveEntry(cmd, process.argv[4]);
    break;
  case 'star':
    taskmgr.markTasks('star', cmd);
    break;
  case 'unstar':
    taskmgr.markTasks('unstar', cmd);
    break;
  case 'c':
  case 'complete':
    taskmgr.markTasks('complete', cmd);
    break;
  case 'incomplete':
    taskmgr.markTasks('incomplete', cmd);
    break;
  case 'organize':
    taskmgr.organize();
    break;
  case 's':
  case 'show':
    switch(cmd) {
      case "a":
      case "all":
        show.showTasks('');
        break;
      case "active":
      case "complete":
        show.showTasks(cmd);
        break;
      case "full":
        show.showFullTasks(process.argv[4]);
        break;
      case "priority":
        show.showByPriority();
        break;
      case 'tags':
        if ( process.argv[4] == null || process.argv[4] == "" )
          show.showAllByTags();
        else
          show.searchTask(process.argv[4], [cmd]);
        break;
      case "today":
        show.showComingNDays(0);
        break;
      case "3":
        show.showComingNDays(2);
        break;
      case "days":
        show.showComingNDays(process.argv[4]);
        break;
      case "raw":
        console.log(taskmgr.data);
        break;
      default:
        if ( cmd === '' || cmd == null )
          show.showTasks('active');
        else {
          console.log('Unknown Argument. Showing help...');
          usage();
        }
    }
    break;
  case 'search':
    show.searchTask(cmd, process.argv[3]);
    break;
  case 'debug':
    debug();
    break;
  default:
    if ( ! fs.existsSync(config.taskLocation) )
      usage();
    else
      show.showTasks('active');
}

function debug() {
  console.log('Location of tasks.json: ' + config.taskLocation);
}
