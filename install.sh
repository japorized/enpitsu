#!/usr/bin/env bash

rm -drf dist/
mkdir -p dist/
cp -R lib -t dist/
cp enpitsu.js package.json package-lock.json README.md LICENSE -t dist/
echo -ne "#!/usr/bin/env bash\n\n\$(which node) $1/lib/enpitsu/enpitsu.js \"\$@\"" > $1/enpitsu
