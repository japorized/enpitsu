const chalk = require('chalk');
const utils = require('./utils');
const TaskManager = require('./classes/taskmanager');

const data = new TaskManager().data;

function showTask(task, appended = "") {
  var indicatePriority = '     ';
  if ( task.priority != 0 ) {
    indicatePriority = "(   )";
    for ( j = 1 ; j <= 4 - task.priority ; j++ ) {
      indicatePriority = utils.replaceCharAtIndex(indicatePriority, j, "!");
    }
  }
  process.stdout.write(chalk`  {gray ${task.id}} {yellow ${task.star ? '*' : ' '}} {red ${indicatePriority}} {whiteBright ${task.name}} {gray ${task.stat == "completed" ? "(completed)" : ''}}   ${appended}\n`);
}

function showFullTask(task) {
  showTask(task);
  console.log(chalk`  {magenta Tags}      : ${task.tags}`);
  if ( task.start != 'none' ) {
    console.log(chalk`  {magenta Duration}  : ${new Date(task.start).toLocaleString('en-GB')} - ${new Date(task.end).toLocaleString('en-GB')}`);
  }
  console.log(chalk`  {magenta Status}    : ${task.stat}`);
  if ( task.notes != '' ) {
    console.log(chalk`  {magenta Notes}     :`);
    console.log(`${task.notes}`);
  }
}

function showTasks(arg) {
  for ( var key in data ) {
    if ( key == 0 ) continue;
    if ( data[key].inUse ) {
      var wantedActiveTask = ( arg === 'active' && data[key].stat === 'active' ),
          wantedCompletedTask = ( arg === 'complete' && data[key].stat === 'completed' ),
          allWanted = ( arg === '' ),
          shouldPrint = wantedActiveTask || wantedCompletedTask || allWanted;
      if ( shouldPrint ) {
        showTask(data[key]);
      }
    }
  }

  console.log(chalk`\n   {green Active}: ${data[0].active}   {cyan Completed}: ${data[0].completed}   {yellow Starred}: ${data[0].starred}`);
}

function showFullTasks(arg) {
  
  if ( arg === '' || arg == null ) {
    for ( var i = 1; i < Object.keys(data).length ; i++ ) {
      if ( data[i].inUse ) {
        showFullTask(data[i]);
        console.log('');
      }
    }
  } else {
    var array = utils.humanListToArray(arg);
    for ( i = 0 ; i < array.length ; i++ ) {
      showFullTask(data[array[i]]);
    }
  }
  console.log(chalk`\n   {green Active}: ${data[0].active}   {cyan Completed}: ${data[0].completed}   {yellow Starred}: ${data[0].starred}`);
}

function showToday() {
  var today = new Date().getTime();
  for ( var key in data ) {
    if ( key == 0 ) continue;
    else if ( ! data[key].inUse || data[key].start == "none" ) continue;

    var start = new Date( data[key].start ).getTime(),
        end = new Date( data[key].end ).getTime();

    if ( start <= today && today <= end )
      showTask(data[key]);
  }
}

function showComingNDays(days) {
  var range = {
    start : new Date().getTime(),
    end : new Date().setDate(new Date().getDate() + days),
  };
  var cabinet = [];
  for ( var key in data ) {
    if ( key == 0 ) continue;
    else if ( !data[key].inUse || data[key].start == "none" ) continue;

    var taskStartDate = new Date( data[key].start ).getTime(),
        taskEndDate = new Date( data[key].end ).getTime();
    var endsInRange = ( taskStartDate <= range.start && range.start <= taskEndDate ),
        beginsInRange = ( taskStartDate <= range.end && range.end <= taskEndDate ),
        isInRange = ( range.start <= taskStartDate && taskEndDate <= range.end );

    if ( endsInRange || beginsInRange || isInRange )
      showTask(data[key]);
  }
}

async function searchTask(query, filter = ["name", "tags", "notes"], status = 'active', fuzzy = true) {
  var results = await utils.search(data, query, filter, status, fuzzy);

  // making the length of all titles the same by appending whitespaces
  var maxTitleLength = 0;
  for ( var i = 0 ; i < results.length ; i++ ) {
    if ( maxTitleLength < results[i].name.length )
      maxTitleLength = results[i].name.length;
  }
  for ( var i = 0 ; i < results.length ; i++ ) {
    if ( results[i].name.length < maxTitleLength ) {
      var diff = maxTitleLength - results[i].name.length;
      for ( var j = 0 ; j < diff ; j++ ) {
        results[i].name += " ";
      }
    }
  }

  console.log('You searched for: ' + query);
  for ( var i = results.length - 1 ; i >= 0 ; i-- ) {
    showTask(results[i], `(score: ${results[i].score})`);
  }
}

function showByPriority() {
  var cabinet = [[], [], [], []];

  for ( var i = 1 ; i < Object.keys(data).length ; i++ ) {
    if ( data[i].inUse )
      cabinet[data[i].priority].push(data[i]);
  }

  for ( var i = 1 ; i < cabinet.length ; i++ ) {
    if ( cabinet[i].length == 0 ) continue;
    for ( var j = 0 ; j < cabinet[i].length ; j++ ) {
      showTask(cabinet[i][j]);
    }
    console.log(''); // empty line for aesthetics
  }

  if ( cabinet[0].length != 0 ) {
    for ( var j = 0 ; j < cabinet[0].length ; j++ ) {
      showTask(cabinet[0][j]);
    }
  }
}

function showAllByTags() {
  var cabinet = {};
  for ( var key in data ) {
    if ( key == 0 || ! data[key].inUse ) {
      continue;
    }
    for ( var i = 0 ; i < data[key].tags.length ; i++ ) {
      var tag = data[key].tags[i];
      if ( ! Array.isArray( cabinet[tag] ) ) {
        cabinet[tag] = new Array();
      }
      cabinet[tag].push(data[key]);
    }
  }

  for ( var key in cabinet ) {
    console.log(` ${style.bold}${style.rev}${style.mgt} ${key} ${style.crst}${style.nrev}${style.frst}`);
    for ( var i = 0 ; i < cabinet[key].length ; i++ ) {
      showTask(cabinet[key][i]);
    }
    console.log(''); // empty line for aesthetics
  }
}

module.exports = {
  showTask: showTask,
  showFullTask: showFullTask,
  showTasks: showTasks,
  showFullTasks: showFullTasks,
  showToday: showToday,
  showComingNDays: showComingNDays,
  searchTask: searchTask,
  showByPriority: showByPriority,
  showAllByTags: showAllByTags
}
