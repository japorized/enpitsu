const chalk = require('chalk');

module.exports = Object.freeze({
  app             : chalk`{bgMagenta  Enpitsu }`,
  complete        : chalk`{green Completed}`,
  protectedID     : chalk`{red id 0 is a protected target!}`
});
