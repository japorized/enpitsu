const fs = require('fs');
const spawn = require('child_process').spawnSync;
const qoa = require('qoa');
const chalk = require('chalk');
const utils = require('../utils');

class Task {
  constructor() {
    this.id = -2;
    this.inUse = true;
    this.name = '';
    this.start = '';
    this.end = '';
    this.tags = new Array();
    this.priority = 0;
    this.notes = '';
    this.stat = 'active';
    this.star = false;
    this.creation = new Date();
  }

  get startDate() {
    return this.start.toDateString();
  }

  get startTime() {
    return this.start.toTimeString();
  }

  get endDate() {
    return this.end.toDateString();
  }

  get endTime() {
    return this.end.toTimeString();
  }

  async setID() {
    
  }

  async setName(name = '') {
    var _this = this;
    if ( name == null || name == '' ) {
      var queryName = await qoa.input({
        type: 'input',
        query: chalk`{magenta Title}:`,
        handle: 'name'
      }).then(function(val) {
        _this.name = val.name;
      });
    } else {
      _this.name = name;
    }
  }

  async setTargetDate(type) {
    // type can only be either start or end
    if ( type != "start" && type != "end" ) {
      console.error('type of setTargetDate can only be either start or end.');
      process.exit(1);
    }

    const _this = this;
    var dateSet = await qoa.input({
      type: 'input',
      query: chalk`{magenta ${utils.toProperCase(type)} Date}:`,
      handle: 'date'
    }).then(function(val) {
      _this._setDate(val, type);
    });
  }

  _setDate(val, type) {

    if ( val.date == "none" ) {
      this[type] = 'none';
    } else if ( val.date == "" ) {
      this[type] = new Date();
      console.log('Date set to ' + this[type]);
    } else {
      try {
        var result = new Date(val.date);
        // Safeguard against lazy entries on the year
        // I certainly do not want to type the year every time I
        // create a new entry
        if ( result.getFullYear() < new Date().getFullYear() ) {
          if ( result.getMonth() < new Date().getMonth() )
            result.setFullYear(new Date().getFullYear() + 1);
          else
            result.setFullYear(new Date().getFullYear());
        }

        this[type] = result;
      } catch (e) {
        console.error(val.date + ' is not a proper date!');
        console.log('Entry not created');
        process.exit(1);
      }
    }
  }

  async setTags() {
    const _this = this;
    var answer = await qoa.input({
      type: 'input',
      query: chalk`{magenta Tags} (comma-separated):`,
      handle: 'tags'
    });
    if ( answer.tags == null || answer.tags == "" ) {
      _this.tags = new Array();
    } else {
      _this.tags = answer.tags.split(',');
    }
  }

  async editTags() {
    console.log('Editing tags for task ' + this.name);
    console.log('Current tags: ' + this.tags);
    var answer = await qoa.input({
      type: 'input',
      query: chalk`{magenta Tags}:`,
      handle: 'tags'
    });

    var tags = answer.tags.split(',');

    // set starting mode
    var mode = "replace";
    if ( tags[0].indexOf('+') == 0 ) {
      mode = "append";
      tags[0] = tags[0].substring(1, tags[0].length);
    }
    else if ( tags[0].indexOf('-') == 0 ) {
      mode = "remove";
      tags[0] = tags[0].substring(1, tags[0].length);
    }

    if ( mode == "replace" ) {
      this.tags = tags;
    } else {
      // in this block, mode is always either append or remove
      for ( var tag of tags ) {
        if ( tag.indexOf('+') == 0 && mode == "remove" ) {
          mode = "append";
          tag = tag.substring(1, tag.length);
        } else if ( tag.indexOf('-') == 0 && mode == "append" ) {
          mode = "remove";
          tag = tag.substring(1, tag.length);
        }

        if ( mode == "append" ) {
          this.tags.push(tag);
        } else if ( mode == "remove" ) {
          var tagIndex = this.tags.indexOf(tag);
          if ( tagIndex > -1 ) {
            this.tags.splice(tagIndex, 1);
          }
        }
      }
    }
  }

  async setPriority(newPriority = -1) {
    if ( newPriority >= 0 && newPriority <= 3 ) {
      this.priority = newPriority;
    } else {
      var answer = await qoa.interactive({
        type: 'interactive',
        query: chalk`{magenta Priority}:`,
        handle: 'priority',
        symbol: '>',
        menu: [ '0', '1', '2', '3' ]
      });
      this.priority = parseInt(answer.priority);
    }
  }

  async setNotes() {
    var answer = await qoa.input({
      type: 'input',
      query: chalk`{magenta Notes}:`,
      handle: "notes"
    });
    this.notes = answer.notes;
  }

  async editNotes(editor) {
    var tmpfilePath = '/tmp/enpitsu-edit-notes-id-' + this.id;
    await fs.writeFileSync(tmpfilePath, this.notes, 'utf8');
    var editorSpawn = spawn(editor, [tmpfilePath], {
      stdio: 'inherit'
    });
    var newnote = fs.readFileSync(tmpfilePath, 'utf8');
    if ( this.notes == newnote ) {
      console.log('Nothing changed.');
      fs.unlink(tmpfilePath, (err) => { if (err) throw err; });
      process.exit(0);
    } else {
      this.notes = fs.readFileSync(tmpfilePath, 'utf8');
      fs.unlink(tmpfilePath, (err) => { if (err) throw err; });
    }
  }

  _nullTask() {
    this.name = '';
    this.start = '';
    this.end = '';
    this.tags = [];
    this.priority = 0;
    this.notes = '';
    this.stat = '';
    this.star = false;
    this.creation = '';
  }

  notInUse() {
    this.inUse = false;
  }

  remove() {
    this._nullTask();
    this.notInUse();
  }

  // @param Object: Task
  copyInfo(task, shouldCopyId = false) {
    if ( shouldCopyId ) {
      this.id = task.id;
    }
    this.inUse = task.inUse;
    this.name = task.name;
    this.start = task.start;
    this.end = task.end;
    this.tags = task.tags;
    this.priority = task.priority;
    this.notes = task.notes;
    this.stat = task.stat;
    this.star = task.star;
    this.creation = task.creation;
  }
}

module.exports = Task;
