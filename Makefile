# enpitsu - terminal pencil for your tasks
# See LICENSE file for copyright and license details
.POSIX:

DEST = /home/$(USER)/.bin
LIB = $(DEST)/lib/enpitsu

install:
	chmod +x ./install.sh
	./install.sh $(DEST)
	echo "Copying configuration file to $(XDG_CONFIG_HOME)/enpitsu"
	mkdir -p $(XDG_CONFIG_HOME)/enpitsu
	cp -f config.json $(XDG_CONFIG_HOME)/enpitsu/config.json
	echo "Making a directory for enpitsu in $(XDG_DATA_HOME)"
	mkdir -p $(XDG_DATA_HOME)/enpitsu
	echo "Installing enpitsu to $(DEST)"
	chmod +x $(DEST)/enpitsu
	ln -sf $(DEST)/enpitsu $(DEST)/enp
	echo "Change directory to $(LIB)"
	echo "Installing dependencies..."
	mkdir -p $(LIB)
	cp -R dist/* -t $(LIB)
	cd $(LIB) && npm install
	echo ""
	echo "Enpitsu has been installed! (Wait, you can install a pencil in the terminal!?)"
	echo "[Reminder] Make sure that $(DEST) is in your PATH environment."

uninstall:
	rm -drf $(DEST)/enpitsu $(DEST)/enp $(LIB)

dev-auto:
	chmod +x ./install.sh
	./install.sh $(DEST)
	mkdir -p $(LIB)
	cp -R dist/* -t $(LIB)

dev:
	ls enpitsu.js lib/**/*.js | entr -s "make dev-auto"

ifndef VERBOSE
.SILENT: install
endif
.PHONY: install uninstall
